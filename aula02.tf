terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

provider "azurerm" {
  features {

  }
}

resource "azurerm_resource_group" "aula02" {
  name     = var.resource_group
  location = "West Europe"
}

resource "azurerm_network_security_group" "network_security_group" {
  name                = "nsg"
  location            = azurerm_resource_group.aula02.location
  resource_group_name = azurerm_resource_group.aula02.name

  security_rule {
    name                       = "SSH"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "MYSQL"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3306"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_virtual_network" "virtual_network" {
  name                = "virtualNetwork1"
  location            = azurerm_resource_group.aula02.location
  resource_group_name = azurerm_resource_group.aula02.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "subnet" {
  name                 = "acctsub"
  resource_group_name  = azurerm_resource_group.aula02.name
  virtual_network_name = azurerm_virtual_network.virtual_network.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "public_ip" {
  name                    = "pip"
  location                = azurerm_resource_group.aula02.location
  resource_group_name     = azurerm_resource_group.aula02.name
  allocation_method       = "Static"
}

resource "azurerm_network_interface" "network_interface" {
  name                = "nic"
  location            = azurerm_resource_group.aula02.location
  resource_group_name = azurerm_resource_group.aula02.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.public_ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "network_interface_security_group_association" {
    network_interface_id      = azurerm_network_interface.network_interface.id
    network_security_group_id = azurerm_network_security_group.network_security_group.id
}

resource "azurerm_virtual_machine" "minha_vm" {
  name                  = "minhaVM"
  location              = azurerm_resource_group.aula02.location
  resource_group_name   = azurerm_resource_group.aula02.name
  network_interface_ids = [azurerm_network_interface.network_interface.id]
  vm_size               = "Standard_DS1_v2"

  delete_os_disk_on_termination = true

  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "meuComputador"
    admin_username = var.user
    admin_password = var.password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

}

data "azurerm_public_ip" "ip_aula02" {
  name                = azurerm_public_ip.public_ip.name
  resource_group_name = azurerm_resource_group.aula02.name
}

resource "time_sleep" "wait_30_seconds" {
  depends_on = [azurerm_virtual_machine.minha_vm]
  create_duration = "30s"
}

resource "null_resource" "install_mysql" {
    triggers = {
        order = azurerm_virtual_machine.minha_vm.id
    }
    provisioner "remote-exec" {
        connection {
            type = "ssh"
            user = var.user
            password = var.password
            host = azurerm_public_ip.public_ip.ip_address
        }
        inline = [
            "sudo apt-get update",
            "sudo echo mysql-server mysql-server/root_password password aulaTerraform | sudo debconf-set-selections",
            "sudo echo mysql-server mysql-server/root_password_again password aulaTerraform | sudo debconf-set-selections",
            "sudo apt-get -y install mysql-server",
            "sudo sed -i \"s/.*bind-address.*/bind-address = 0.0.0.0/\" /etc/mysql/mysql.conf.d/mysqld.cnf",
            "sudo mysql -uroot -paulaTerraform -e \"grant all privileges on *.* to 'root'@'%' identified by 'aulaTerraform'\"",
            "sudo service mysql restart",
            "sleep 20"
        ]
    }

    depends_on = [time_sleep.wait_30_seconds]
}

output "public_ip_address" {
  value = azurerm_public_ip.public_ip.ip_address
}