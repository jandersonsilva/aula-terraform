variable "resource_group" {
  type = string
}

variable "user" {
  type = string
}

variable "password" {
  type = string
}